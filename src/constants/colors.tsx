export const themeColors = {
  cardinal: "#AD343E",
  grey: "#474747",
  yellow: "#f2af29",
  black: "#000",
  eggshell: "#e0e0ce",
};

export const theme2 = {
  byzantium: "#820263",
  rose: "#D90368",
  platinum: "#EADEDA",
  cadet: "#2E294E",
  yellow: "#FFD400",
};

export const theme3 = {
  black: "#011627",
  red: "#F71735",
  turquoise: "#41EAD4",
  white: "#FDFFFC",
  orange: "#FF9F1C",
};
