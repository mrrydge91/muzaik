import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect, withRouter } from 'react-router-dom';

const PublicRoute = ({component: Component, additionalProps, authed, ...rest}) => {

    return (
        <Route
            {...rest}
            render={props => !authed
                ? <Component {...props} />
                : <Redirect to={{
                    pathname: "/profile/1",
                    state: {from: props.location}
                }}/>}
        />
    );
};


const mapStateToProps = state => ({
    user: state.user,
});


export default withRouter(connect(mapStateToProps)(PublicRoute))
