import firebase from "../config/firebaseConfig";

const FirebaseUtils = {
  auth: {},

  firestore: {
    addComment: (productId, newComments) => {
      firebase
        .firestore()
        .collection("products")
        .doc(productId)
        .update({ comments: newComments })
        .catch((error) => console.error("ADD COMMENT", error));
    },
    addEventComment: (eventId, newComments) => {
      firebase
        .firestore()
        .collection("events")
        .doc(eventId)
        .update({ comments: newComments })
        .catch((error) => console.error("ADD COMMENT", error));
    },

    addEventToUser: (userUID, eventId) => {
      firebase
        .firestore()
        .collection("users")
        .doc(userUID)
        .update({
          events: firebase.firestore.FieldValue.arrayUnion(eventId),
        });
    },

    getUserEvents: async (userUID) => {
      const docs = [];
      await firebase
        .firestore()
        .collection("events")
        .where("userUID", "==", userUID)
        .get()
        .then((resp) => {
          resp.forEach((doc) => {
            docs.push(doc.data());
          });
        });
      return docs;
    },
  },

  storage: {},
};

export default FirebaseUtils;
