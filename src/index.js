import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import store from "./store";
import { Provider } from "react-redux";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";

if (process.env.NODE_ENV === "development") {
  window.store = store;
}

const theme = createMuiTheme({
  overwrite: {
    MuiButtons: {
      "&:hover": {
        color: "#fff",
        background:
          "linear-gradient(red, transparent), linear-gradient(to top left, purple, transparent), linear-gradient(to top right, blue, transparent)",
        backgroundBlendMode: "screen",
      },
    },
  },
});

ReactDOM.render(
  <Provider store={store}>
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </MuiPickersUtilsProvider>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
