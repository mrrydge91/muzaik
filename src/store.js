import { createStore, applyMiddleware, compose } from "redux";
import reducers from "./reducers";
import { register } from "./middleware/register";
import { login } from "./middleware/login";
import { profile } from "./middleware/profile";
import { product } from "./middleware/product";
import { events } from "./middleware/events";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducers,
  composeEnhancers(applyMiddleware(login, register, profile, product, events))
);

export default store;
