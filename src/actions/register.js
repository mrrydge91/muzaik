import { REGISTER_NEW_USER } from "../actionTypes/register";
import firebase from "../config/firebaseConfig";

export function registerNewUser(user) {
  firebase.firestore().collection(`/users`).doc(`/${user.uid}`).set(user);
  return {
    type: REGISTER_NEW_USER,
    user,
  };
}
