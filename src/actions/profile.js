import { ADD_PIC, SET_PROFILE, EDIT_PROFILE } from "../actionTypes/profile";

export function setProfile(profile) {
  return {
    type: SET_PROFILE,
    profile
  };
}

export function addPic(pic, uid) {
  return {
    type: ADD_PIC,
    pic,
    uid
  };
}

export function editProfile(profile) {
  return {
    type: EDIT_PROFILE,
    profile
  };
}
