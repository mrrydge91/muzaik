import { CHANGE_TAB_VALUE } from "../actionTypes/appbar";

export const setTabValue = (tabValue) => {
  return {
    type: CHANGE_TAB_VALUE,
    tabValue,
  };
};
