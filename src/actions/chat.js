import { CREATE_CHAT, ADD_MESSAGE } from "../actionTypes/chat";

export const createChat = (chat) => {
  return {
    type: CREATE_CHAT,
    chat,
  };
};

export const addMessage = (message) => {
  return {
    type: ADD_MESSAGE,
    message,
  };
};
