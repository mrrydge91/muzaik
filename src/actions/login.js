import { LOGIN, LOGOUT, UPDATE_USER } from "../actionTypes/login";

export function login(email, uid) {
  return {
    type: LOGIN,
    email,
    uid
  };
}

export function logout() {
  return {
    type: LOGOUT
  };
}

export function updateUser(uid) {
  return {
    type: UPDATE_USER,
    uid
  };
}
