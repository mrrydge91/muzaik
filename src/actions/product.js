import { ADD_PRODUCT, SET_PRODUCT, EDIT_PRODUCT } from "../actionTypes/product";

export function addProduct(product) {
  return {
    type: ADD_PRODUCT,
    product
  };
}

export function setProduct(product) {
  return {
    type: SET_PRODUCT,
    product
  };
}

export function editProduct(product) {
  return {
    type: EDIT_PRODUCT,
    product
  };
}
