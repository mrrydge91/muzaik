import {
  ADD_EVENT,
  CANCEL_EVENT,
  EDIT_EVENT,
  SET_EVENT,
} from "../actionTypes/events";

export const addEvent = (event) => {
  return {
    type: ADD_EVENT,
    event,
  };
};

export const setEvent = (event) => {
  return {
    type: SET_EVENT,
    event,
  };
};

export const editEvent = (event) => {
  return {
    type: EDIT_EVENT,
    event,
  };
};

export const cancelEvent = (eventId, productId) => {
  return {
    type: CANCEL_EVENT,
    eventId,
    productId,
  };
};
