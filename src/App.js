import React, { useEffect, useCallback } from "react";
import "./App.css";
import { connect } from "react-redux";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
import LoginFormContainer from "./containers/loginFormContainer";
import MainContainer from "./containers/mainContainer";
import RegisterFormContainer from "./containers/registerFormContainer";
import firebase from "./config/firebaseConfig";
import { login, logout, updateUser } from "./actions/login";
import { registerNewUser } from "./actions/register";
import ProfileContainer from "./containers/profileContainer";
import ProductContainer from "./containers/productContainer";
import EventsContainer from "./containers/eventsContainer";
import BulletinContainer from "./containers/bulletinContainer";
import AboutContainer from "./containers/aboutContainer";
import AppbarContainer from "./containers/appbarContainer";
import Footer from "./components/footer";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  main: { marginTop: "5%" },
});

const App = ({ login, updateUser, registerNewUser, user, logout }) => {
  const classes = useStyles();
  const setupUser = useCallback(
    (email, uid) => {
      login(email, uid);
      updateUser(uid);
    },
    [login, updateUser]
  );

  const registerCallback = useCallback(
    (registeredUser) => {
      registerNewUser(registeredUser);
    },
    [registerNewUser]
  );

  useEffect(() => {
    firebase
      .auth()
      .getRedirectResult()
      .then(function (result) {
        if (result.user || firebase.auth().currentUser) {
          let user;
          if (result.user) {
            user = result.user;
          } else {
            user = firebase.auth().currentUser;
          }

          firebase
            .firestore()
            .collection("users")
            .doc(`${user.uid}`)
            .get()
            .then((doc) => {
              if (doc.exists) {
                setupUser(user.email, user.uid);
              } else if (result.user) {
                registerCallback({
                  uid: result.user.uid,
                  email: result.user.email,
                  name: result.user.displayName,
                  pic: result.user.photoURL,
                  bio: "",
                  hometown: "",
                });
                return <Redirect to={`/profile/${user.uid}`} />;
              } else {
                alert("There is no user on file");
              }
            });
        }
      })
      .catch(function (error) {
        let errorMessage = error.message;
        alert(errorMessage);
      });
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        setupUser(user.email, user.uid);
      } else {
        console.log("no user signed in");
      }
    });
  }, [setupUser, registerCallback]);

  return (
    <div style={{ height: "100vh" }}>
      <Router>
        <AppbarContainer />
        <div className={classes.main}>
          <Switch>
            <Route path={"/profile/:id"}>
              <ProfileContainer />
            </Route>

            <Route path={"/main"}>
              <MainContainer />
            </Route>

            <Route path={"/bulletinboard"}>
              <BulletinContainer />
            </Route>

            <Route path={"/register"}>
              <RegisterFormContainer />
            </Route>
            <Route path={"/event/:eventId"}>
              <EventsContainer />
            </Route>
            <Route path={"/product/:postId"}>
              <ProductContainer />
            </Route>
            <Route path={"/login"}>
              <LoginFormContainer />
            </Route>
            <Route path={"/about"}>
              <AboutContainer />
            </Route>
            <Route exact path={"/"}>
              <Redirect to="/main" />
            </Route>
          </Switch>
        </div>
        <div style={{ position: "fixed", bottom: 0, width: "100%" }}>
          <Footer />
        </div>
      </Router>
    </div>
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  login: (email, uid) => dispatch(login(email, uid)),
  registerNewUser: (state) => dispatch(registerNewUser(state)),
  logout: () => dispatch(logout()),
  updateUser: (uid) => dispatch(updateUser(uid)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
