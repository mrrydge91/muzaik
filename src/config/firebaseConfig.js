import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";
import "firebase/storage";


const config = {
  apiKey: process.env.REACT_APP_MUZAIK_API_KEY,
  authDomain: process.env.REACT_APP_MUZAIK_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_MUZAIK_DATABASE_URL,
  projectId: process.env.REACT_APP_MUZAIK_PROJECT_ID,
  storageBucket: process.env.REACT_APP_MUZAIK_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MUZAIK_MESSAGING_SENDER_ID,
};
firebase.initializeApp(config);

export default firebase;
