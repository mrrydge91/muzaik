import { LOGIN, LOGOUT, UPDATE_USER } from "../actionTypes/login";
import firebase from "../config/firebaseConfig";

export const login = (store) => (next) => (action) => {
  switch (action.type) {
    case LOGIN:
      return next(action);
    case UPDATE_USER:
      firebase
        .firestore()
        .collection("users")
        .doc(action.uid)
        .get()
        .then((resp) => {
          const finalAction = action;
          finalAction.user = resp.data();
          return next(finalAction);
        });
      break;
    case LOGOUT:
      firebase.auth().signOut();
      return next(action);
    default:
      return next(action);
  }
};
