import firebase from "../config/firebaseConfig";
import { ADD_PRODUCT, EDIT_PRODUCT } from "../actionTypes/product";

export const product = (store) => (next) => (action) => {
  switch (action.type) {
    case ADD_PRODUCT:
      firebase
        .firestore()
        .collection("products")
        .add(action.product)
        .then((resp) => {
          firebase
            .firestore()
            .collection("products")
            .doc(`${resp.id}`)
            .update({ postKey: resp.id });
          firebase
            .firestore()
            .collection("users")
            .doc(`${action.product.user}`)
            .update({
              products: firebase.firestore.FieldValue.arrayUnion({
                name: action.product.name,
                postKey: resp.id,
                type: action.product.type,
                role: "creator",
              }),
            });
        });
      return next(action);
    case EDIT_PRODUCT:
      const product = action.product;
      const postKey = action.product.postKey;
      firebase
        .firestore()
        .collection("products")
        .doc(`${postKey}`)
        .update(product);

      firebase
        .firestore()
        .collection("users")
        .doc(`${product.user}`)
        .get()
        .then((resp) => {
          const allProducts = resp.data().products || [];
          allProducts.forEach((loopProd, idx) => {
            if (loopProd.postKey === postKey) {
              allProducts[idx] = {
                name: product.name,
                postKey: product.postKey,
                type: product.type,
              };
            }
          });

          firebase
            .firestore()
            .collection("users")
            .doc(`${product.user}`)
            .update({ products: allProducts });
          return next(action);
        });
      return next(action);
    default:
      next(action);
  }
};
