import { REGISTER_NEW_USER } from "../actionTypes/register";
import firebase from "../config/firebaseConfig";

export const register = (store) => (next) => (action) => {
  let result = next(action);
  if (action.type === REGISTER_NEW_USER) {
    firebase
      .firestore()
      .collection("users")
      .doc(`${action.user.uid}`)
      .set(action.user);
  }
  return result;
};
