import firebase from "../config/firebaseConfig";
import { ADD_PIC, EDIT_PROFILE } from "../actionTypes/profile";

export const profile = (store) => (next) => (action) => {
  switch (action.type) {
    case ADD_PIC:
      firebase.firestore().collection(`users`).doc(`${action.uid}`).update({
        pic: action.pic,
      });
      return next(action);

    case EDIT_PROFILE:
      firebase
        .firestore()
        .collection(`/users`)
        .doc(`${action.profile.uid}`)
        .update(action.profile);
      return next(action);

    default:
      return next(action);
  }
};
