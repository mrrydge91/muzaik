import { ADD_EVENT, EDIT_EVENT, CANCEL_EVENT } from "../actionTypes/events";
import firebase from "../config/firebaseConfig";
// import FirebaseUtils from "../utils/firebaseUtils";

export const events = (store) => (next) => (action) => {
  let result = next(action);
  switch (action.type) {
    case ADD_EVENT:
      if (!action.event.postKey) {
        firebase
          .firestore()
          .collection("events")
          .add(action.event)
          .then((resp) => {
            if (action.event.productId) {
              firebase
                .firestore()
                .collection("products")
                .doc(`${action.event.productId}`)
                .update({
                  events: firebase.firestore.FieldValue.arrayUnion({
                    postKey: resp.id,
                    name: action.event.name,
                    date: action.event.date,
                  }),
                });
            }
            firebase
              .firestore()
              .collection("events")
              .doc(`${resp.id}`)
              .update({ postKey: resp.id });
          });
      }
      return result;
    case EDIT_EVENT:
      firebase
        .firestore()
        .collection("events")
        .doc(`${action.event.postKey}`)
        .update(action.event);
      return result;
    case CANCEL_EVENT:
      firebase
        .firestore()
        .collection("events")
        .doc(`${action.eventId}`)
        .update({ deleted: true });
      return result;
    default:
      return result;
  }
};
