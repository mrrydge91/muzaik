import { Card, CardContent, CardHeader } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import FirebaseUtils from "../utils/firebaseUtils";

const UserEvents = (props) => {
  const { userUID } = props;
  const [allEvents, setAllEvents] = useState([]);

  useEffect(() => {
    const getUserEvents = async () => {
      const events = await FirebaseUtils.firestore.getUserEvents(userUID);
      setAllEvents(events);
    };

    getUserEvents();
  }, [userUID]);

  return (
    <Card>
      <CardHeader title="Events" />
      <CardContent>
        {allEvents.map((event) => {
          return (
            <Card key={event.date}>
              <CardContent>
                <Link to={`/event/${event.postKey}`}> {event.name}</Link>
              </CardContent>
            </Card>
          );
        })}
      </CardContent>
    </Card>
  );
};

export default UserEvents;
