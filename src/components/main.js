import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { Card, CardContent } from "@material-ui/core";

const styles = makeStyles({
  logoLetters: {
    fontFamily: "Courier New, Courier, monospace",
    fontWeight: 900,
    fontSize: "5em",
  },
  courier: {
    fontFamily: "Courier New, Courier, monospace",
    fontWeight: 600,
  },
  header: {
    fontFamily: "Courier New, Courier, monospace",
    fontWeight: 600,
    fontSize: "3em",
    margin: "50px auto 0 auto",
  },
  mainCard: {
    height: "100vh",
    fontFamily: "Courier New, Courier, monospace",
    fontWeight: 600,
    textAlign: "center",
  },
  ul: {
    textAlign: "left",
  },
});

const Main = () => {
  const classes = styles();
  return (
    <Card className={classes.mainCard}>
      <CardContent>
        <Grid container>
          <Grid item xs={12}>
            <Typography align={"center"} className={classes.logoLetters}>
              Muzaik
            </Typography>
          </Grid>
          <Grid item xs={2} />
          <Grid item xs={8}>
            <p>
              Muzaik is a place to show off your creative projects. This
              project's goal is to celebrate the creatives of Pittsburgh. We
              work damn hard and all for other people to enjoy. This is for us.
              My name is Kevin Ridge and I built this website. I wanted to show
              off the things I've made while encouraging artists to collaborate
              all over the city. I love local art and I want to show my
              gratitude to all of the invisible work that artists do.{" "}
            </p>
            <br />
            <br />
            <p>
              This project is very bare bones as of now (I'm only one guy!) but
              here are some ideas of what's coming:
            </p>
            <ul className={classes.ul}>
              <li>Upload progress pictures of your projects</li>
              <li>Chat feature (ya know, like AIM)</li>
              <li>
                Video chat (with shared docs, presenting modes, notes section)
              </li>
            </ul>
            <br />
            <br />
            <h1>What I need help with:</h1>
            <ul className={classes.ul}>
              <li>Design</li>
              <li>I need feedback to help the community more</li>
              <li>Pictures from you to use for my stock images</li>
              <li>People signing up!</li>
            </ul>
          </Grid>
          <Grid item xs={2} />
        </Grid>
      </CardContent>
    </Card>
  );
};

export default Main;
