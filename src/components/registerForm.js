import React from "react";
import {
  CardHeader,
  TextField,
  Button,
  Card,
  CardActions,
  CardContent,
} from "@material-ui/core/";

const RegisterForm = ({
  newRegister,
  handlePassword,
  handleText,
  state,
  password,
  google,
}) => {
  return (
    <div>
      <h1>New Register</h1>
      <br />
      <br />
      <br />
      <Card>
        <CardHeader title={"Register"} />
        <CardContent>
          <TextField
            value={state.email}
            onChange={(e) => handleText(e, "email")}
            label={"Your Email"}
          />
          <TextField
            value={password}
            onChange={handlePassword}
            label={"Your Password"}
          />
          <CardActions>
            <Button onClick={newRegister}>Register</Button>
            {/* <Button onClick={google}>Login with Google</Button> */}
          </CardActions>
        </CardContent>
      </Card>
    </div>
  );
};

export default RegisterForm;
