import React from "react";
// import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const AddBulletin = ({ bulletinText, setBulletinText, submit }) => {
  return (
    <div>
      <TextField
        value={bulletinText}
        onChange={(e) => setBulletinText(e.target.value)}
      />

      <Button onClick={submit}>Submit</Button>
    </div>
  );
};

export default AddBulletin;
