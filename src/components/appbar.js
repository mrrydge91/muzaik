import React from "react";
import { useHistory } from "react-router-dom";
import {
  AppBar,
  Button,
  Grid,
  IconButton,
  makeStyles,
  Menu,
  MenuItem,
  Toolbar,
} from "@material-ui/core";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import "../css/spotlight.scss";
import { theme3 } from "../constants/colors";
import SpotlightLogo from "./customButtons/spotlightLogo";
import { logout } from "../actions/login";
import { useDispatch } from "react-redux";

const styles = makeStyles({
  accountIcon: {},
  appbar: {
    backgroundColor: theme3.white,
  },
  toolbar: { minHeight: "0px" },
});

const Appbar = ({ tabValue, handleChange, uid, logout }) => {
  const classes = styles();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const history = useHistory();
  const dispatch = useDispatch();

  return (
    <AppBar className={classes.appbar}>
      <Toolbar className={classes.toolbar}>
        <Grid container>
          <Grid item sm={2}>
            <SpotlightLogo />
          </Grid>
          <Grid item sm={9}>
            <Button onClick={() => history.push("/product/new")}>
              New Product
            </Button>
            <Button onClick={() => history.push("/event/new")}>
              New Event
            </Button>
          </Grid>
          <Grid item sm={1}>
            <IconButton onClick={handleClick} className={classes.accountIcon}>
              <AccountCircleIcon />
            </IconButton>
            <Menu
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              {uid ? (
                <>
                  <MenuItem
                    onClick={() => {
                      history.push(`/profile/${uid}`);

                      handleClose();
                    }}
                  >
                    Profile
                  </MenuItem>
                  <MenuItem
                    onClick={() => {
                      handleClose();
                      dispatch(logout());
                    }}
                  >
                    Logout
                  </MenuItem>
                </>
              ) : (
                <MenuItem
                  onClick={() => {
                    handleClose();
                    history.push("/login");
                  }}
                >
                  Login
                </MenuItem>
              )}
            </Menu>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};

export default Appbar;
