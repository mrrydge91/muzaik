import React from "react";
import { Grid, makeStyles } from "@material-ui/core";

const styles = makeStyles({
  courier: {
    fontFamily: "Courier New, Courier, monospace",
    fontWeight: 800,
  },
  aBitBigger: {
    fontSize: "1.2em",
    fontWeight: 400,
  },
  footer: {
    backgroundColor: "#000",
    color: "#808080",
    textAlign: "center",
  },
  link: {
    color: "#808080",
    textDecoration: "none",
    "&:hover": {
      color: "#fff",
    },
  },
  bottom: {
    bottom: 0,
  },
});

const Footer = () => {
  const classes = styles();
  return (
    <Grid className={classes.bottom} container spacing={4}>
      <Grid item xs={12}>
        <div className={`${classes.footer} ${classes.courier}`}>
          <a className={classes.link} href="https://www.patreon.com/muzaik">
            Patreon
          </a>{" "}
          |{" "}
          <a
            className={classes.link}
            href="https://www.facebook.com/groups/258650631543139"
          >
            Facebook
          </a>
        </div>
      </Grid>
    </Grid>
  );
};
export default Footer;
