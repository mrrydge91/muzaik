import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";
import CameraPic from "../images/annie-spratt-oJQTKv6PH3U-unsplash.jpg";
import LittleCat from "../images/chris-hardy-P5Mjq0XXTH4-unsplash.jpg";
import ColorfulWoman from "../images/enzo-tommasi-wlxJ4idMTUk-unsplash.jpg";
import BuildingsPic from "../images/matthew-t-rader-iKwCEiVKN-A-unsplash.jpg";
import productTypes from "../constants/productTypes";

const picArray = [CameraPic, LittleCat, ColorfulWoman, BuildingsPic];

const useStyles = makeStyles({
  courier: {
    fontFamily: "Courier New, Courier, monospace",
    fontWeight: 600,
  },
  white: {
    color: "#FFF",
  },
  subHeader: {
    fontSize: "2em",
  },
  submitButton: {
    borderRadius: "0px",
    border: "2px solid black",
    fontFamily: "Courier New, Courier, monospace",
    fontWeight: 600,
    "&:hover": {
      color: "#fff",
      background: "#000",
    },
  },

  minHeight: {
    minHeight: "650px",
  },
  products: {
    textAlign: "center",
    marginBottom: "100px",
  },
  productButton: {
    borderRadius: "5px",
    width: "300px",
    height: "300px",
    "&:hover": {
      background: "#000",
      backgroundBlendMode: "screen",
    },
  },

  productDiv: {
    margin: "0 5px",
  },
  productImage: {
    width: "300px",
    height: "300px",
    position: "absolute",
    zIndex: 0,
    opacity: "60%",
  },
  productName: {
    zIndex: 1,
    backgroundColor: "rgba(255,255,255,0.8)",
    padding: "5px",
  },
  mainCard: {
    width: "100%",
    maxHeight: "90vh",
  },
  cardHeader: {},
  cardContent: {
    overflow: "scroll",
    maxHeight: "80vh",
  },
});

const UserProducts = ({ products }) => {
  const classes = useStyles();
  return (
    <Card className={classes.mainCard}>
      <CardHeader className={classes.cardHeader} title="Things I've made" />
      <CardContent className={classes.cardContent}>
        <Grid container>
          {productTypes.map((type) => {
            if (
              Object.values(products).filter((product) => product.type === type)
                .length
            ) {
              return (
                <Grid key={type} item xs>
                  <Card>
                    <CardHeader title={type} />
                    <CardContent className={classes.cardContent}>
                      {Object.values(products)
                        .filter((product) => product.type === type)
                        .map((filteredProd, index) => {
                          return (
                            <Grid item xs key={filteredProd.postKey + index}>
                              <Link to={`/product/${filteredProd.postKey}`}>
                                <Button
                                  className={`${classes.productButton} ${classes.courier}`}
                                >
                                  <Typography
                                    className={`${classes.productName} ${classes.courier}`}
                                  >
                                    {filteredProd.name}
                                  </Typography>
                                  <img
                                    className={classes.productImage}
                                    src={picArray[index % 4]}
                                    alt={picArray[index % 4]}
                                  />
                                </Button>
                              </Link>
                            </Grid>
                          );
                        })}
                    </CardContent>
                  </Card>
                </Grid>
              );
            } else {
              return null;
            }
          })}
        </Grid>
      </CardContent>
    </Card>
  );
};

export default UserProducts;
