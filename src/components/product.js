import React, { forwardRef } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  TextField,
  Card,
  CardContent,
  CardHeader,
  makeStyles,
  Typography,
  Grid,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Input,
  CardActions,
} from "@material-ui/core";
// import EventsContainer from "../containers/eventsContainer";
// import ChatContainer from "../containers/chatContainer";
import ReactPlayer from "react-player/lazy";
import MaterialTable from "material-table";
import DeleteIcon from "@material-ui/icons/Delete";
import productType from "../constants/productTypes";
import profilePlaceholder from "../images/profile-placeholder.png";
import AutoCompleteContainer from "../containers/autoComplete";
import WritingTypeContainer from "../containers/writingTypeContainer";
import ProductCommentContainer from "../containers/productCommentContainer";
// import EventsContainer from "../containers/eventsContainer";

const styles = makeStyles({
  courier: {
    fontFamily: "Courier New, Courier, monospace",
    fontWeight: 600,
  },
  black: {
    color: "#000",
  },
  formControl: {
    width: "30%",
  },
  productPic: {
    width: "100%",
  },
  desc: {
    width: "100%",
  },
  role: {
    width: "100%",
  },
  videoWindow: {
    margin: "10px auto",
  },
});

const tableIcons = {
  deleteIcon: forwardRef((props, ref) => <DeleteIcon {...props} ref={ref} />),
};

const Product = ({
  addProduct,
  properties,
  handleTextChange,
  editProduct,
  productId,
  handleCreditPersonTextChange,
  handleCreditRoleTextChange,
  addCredit,
  creditPersonText,
  creditRoleText,
  chat,
  events,
  deleteCredit,
  isUser,
  isNew,
  signedIn,
  addPic,
  setWritingArea,
}) => {
  const classes = styles();
  return (
    <Card>
      <CardHeader
        title={
          <Typography variant="h3" align="center" className={classes.courier}>
            Presenting: {!isNew ? properties.name : "New Project"}
          </Typography>
        }
      />
      <CardContent>
        {properties.videoLink && (
          <Grid item xs={12}>
            <ReactPlayer
              className={classes.videoWindow}
              width={"100%"}
              height="80vh"
              url={properties.videoLink}
            />
          </Grid>
        )}

        <form onSubmit={addProduct}>
          <Grid container>
            <>
              <Grid item xs={4}>
                <Card>
                  <CardContent>
                    {properties.pic && (
                      <img
                        className={classes.productPic}
                        src={properties.pic || profilePlaceholder}
                        alt="product_pic"
                      />
                    )}
                    {(isUser || isNew) && (
                      <Input
                        inputProps={{
                          className: `${classes.courier}`,
                        }}
                        type={"file"}
                        onChange={addPic}
                      />
                    )}
                  </CardContent>
                </Card>
              </Grid>

              <br />
              <Grid item xs={8}>
                <Card>
                  <CardContent>
                    <TextField
                      className={classes.courier}
                      value={properties.name}
                      label={"name"}
                      onChange={(e) => handleTextChange(e, "name")}
                      disabled={!isUser && !isNew}
                      InputProps={{
                        disableUnderline: !isUser,
                        className: `${classes.black} ${classes.courier}`,
                      }}
                    />
                    <br />

                    <FormControl className={classes.formControl}>
                      <InputLabel id="mutiple-name-label">Type</InputLabel>
                      <Select
                        labelId="mutiple-name-label"
                        disabled={!isUser && !isNew}
                        label="Type"
                        id="demo-customized-select"
                        value={properties.type}
                        onChange={(e) => handleTextChange(e, "type")}
                      >
                        {productType.map((type) => {
                          return (
                            <MenuItem key={type} value={type}>
                              {type}
                            </MenuItem>
                          );
                        })}
                      </Select>
                    </FormControl>
                    <br />
                    <TextField
                      className={classes.courier}
                      value={properties.videoLink}
                      label={"Video Link"}
                      onChange={(e) => handleTextChange(e, "videoLink")}
                      disabled={!isUser && !isNew}
                      InputProps={{
                        disableUnderline: !isUser,
                        className: `${classes.black} ${classes.courier}`,
                      }}
                    />
                    <br />
                    <TextField
                      className={classes.courier}
                      value={properties.githubLink}
                      label={"Github Link"}
                      onChange={(e) => handleTextChange(e, "githubLink")}
                      disabled={!isUser && !isNew}
                      InputProps={{
                        disableUnderline: !isUser,
                        className: `${classes.black} ${classes.courier}`,
                      }}
                    />
                    {properties.githubLink && (
                      <a href={properties.githubLink}>
                        <Typography>Go to Project</Typography>
                      </a>
                    )}
                    <br />
                    <TextField
                      className={`${classes.courier} ${classes.desc}`}
                      value={properties.desc}
                      label={"Description"}
                      multiline
                      onChange={(e) => handleTextChange(e, "desc")}
                      disabled={!isUser && !isNew}
                      InputProps={{
                        disableUnderline: !isUser,
                        className: `${classes.black} ${classes.courier}`,
                      }}
                    />
                    {properties.type === "Writing" && (
                      <WritingTypeContainer
                        writingArea={properties.writingArea}
                        setWritingArea={setWritingArea}
                      />
                    )}
                  </CardContent>
                </Card>
              </Grid>
            </>
          </Grid>

          {isUser && productId.length > 0 && (
            <Grid item xs={12}>
              <Card>
                <CardHeader
                  title={
                    <Typography
                      variant="h5"
                      className={classes.courier}
                      align="center"
                    >
                      Add Credits
                    </Typography>
                  }
                />
                <CardContent>
                  <AutoCompleteContainer
                    passedAction={handleCreditPersonTextChange}
                  />
                  <TextField
                    className={classes.role}
                    variant="outlined"
                    value={creditRoleText}
                    onChange={handleCreditRoleTextChange}
                    label="Role"
                  />

                  <CardActions>
                    <Button variant="contained" onClick={addCredit}>
                      Add credit
                    </Button>
                  </CardActions>
                </CardContent>
              </Card>
            </Grid>
          )}
          {properties.credits.length > 0 && (
            <Grid item xs={12}>
              <MaterialTable
                icons={tableIcons}
                title="Credits"
                data={properties.credits}
                columns={[
                  {
                    title: "Name",
                    field: "person",
                    render: (rowData) =>
                      rowData.uid ? (
                        <Link to={`/profile/${rowData.uid}`}>
                          {rowData.person}
                        </Link>
                      ) : (
                        rowData.person
                      ),
                  },
                  { title: "Role", field: "role" },
                ]}
                actions={[
                  {
                    icon: tableIcons.deleteIcon,
                    tooltip: "Delete Credit",
                    onClick: (event, rowData) => {
                      deleteCredit(rowData);
                      // Do save operation
                    },
                  },
                ]}
                options={{
                  actionsColumnIndex: -1,
                }}
              />
            </Grid>
          )}

          {signedIn &&
            (isUser ? (
              <Button variant="contained" onClick={editProduct}>
                Save
              </Button>
            ) : (
              <Button variant="contained" type={"submit"}>
                Submit
              </Button>
            ))}
        </form>
        <br />
        {productId && (
          <Card>
            <CardContent>
              {properties.events?.map((event) => (
                <Card key={event.postKey}>
                  <CardContent>
                    <Link to={`/event/${event.postKey}`}>{event.name}</Link>
                  </CardContent>
                </Card>
              ))}
            </CardContent>
          </Card>
        )}
        {productId && (
          <Link
            to={{
              pathname: "/event/new",
              state: {
                productId: productId,
              },
            }}
          >
            <Button variant="contained">Create Event</Button>
          </Link>
        )}
        {productId && <ProductCommentContainer />}
      </CardContent>
    </Card>
  );
};

export default Product;
