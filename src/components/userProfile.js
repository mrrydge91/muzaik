import React from "react";
import CardActions from "@material-ui/core/CardActions";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Input from "@material-ui/core/Input";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core";
import profilePlaceholder from "../images/profile-placeholder.png";
import UserProducts from "./userProducts";
import { uniqBy } from "lodash";
import UserEvents from "./userEvents";
import FancyButton from "./customButtons/fancyButton";
import MaskButtons from "./customButtons/maskButton";

const styles = makeStyles({
  courier: {
    fontFamily: "Courier New, Courier, monospace",
    fontWeight: 600,
  },
  white: {
    color: "#FFF",
  },
  subHeader: {
    fontSize: "2em",
  },
  submitButton: {
    margin: "auto",
  },

  minHeight: {
    minHeight: "550px",
  },
  products: {
    textAlign: "center",
    marginBottom: "100px",
  },
  productButton: {
    borderRadius: "5px",
    width: "300px",
    height: "300px",
    "&:hover": {
      background: "#000",
      backgroundBlendMode: "screen",
    },
  },

  productDiv: {
    margin: "0 5px",
  },
  productImage: {
    width: "300px",
    height: "300px",
    position: "absolute",
    zIndex: 0,
    opacity: "60%",
  },
  productName: {
    zIndex: 1,
    backgroundColor: "rgba(255,255,255,0.8)",
    padding: "5px",
  },
});

const UserProfile = ({
  profile,
  addPic,
  editField,
  credits,
  bulletins,
  submitProfile,
  products,
  isUser,
}) => {
  const classes = styles();
  const unfilteredProducts = products.concat(credits);
  const allProducts = uniqBy(unfilteredProducts, "name");

  return (
    <Card>
      <CardContent>
        <Grid container>
          <Grid item md={3}>
            <Card className={classes.minHeight}>
              <CardContent>
                <TextField
                  label={"name"}
                  error={isUser && !profile.name}
                  disabled={!isUser}
                  value={profile.name}
                  onChange={(e) => editField(e, "name")}
                  margin="normal"
                  InputProps={{
                    className: `${classes.courier} ${classes.textField}`,
                  }}
                />

                <TextField
                  label={"hometown"}
                  error={isUser && !profile.hometown}
                  disabled={!isUser}
                  value={profile.hometown}
                  onChange={(e) => editField(e, "hometown")}
                  margin="dense"
                  InputProps={{
                    className: `${classes.courier} ${classes.textField}`,
                  }}
                />
                <img
                  style={{ width: "100%" }}
                  src={profile.pic || profilePlaceholder}
                  alt="prof"
                />
                <br />

                {isUser && (
                  <Input
                    inputProps={{
                      className: `${classes.courier}`,
                    }}
                    type={"file"}
                    onChange={addPic}
                  />
                )}
                <TextField
                  style={{ width: "100%" }}
                  label={"bio"}
                  value={profile.bio}
                  onChange={(e) => editField(e, "bio")}
                  margin="normal"
                  multiline
                  InputProps={{
                    disableUnderline: !isUser,
                    className: `${classes.courier}`,
                  }}
                />
              </CardContent>
              <CardActions>
                {isUser && (
                  <div className={classes.submitButton}>
                    <MaskButtons
                      handleClick={submitProfile}
                      text="Save"
                      type="fadeIn"
                    />
                  </div>
                )}
              </CardActions>
            </Card>
          </Grid>

          <Grid item xs={9}>
            <UserProducts products={allProducts} />
          </Grid>
          <Grid item xs={12}>
            <UserEvents userUID={profile.uid} />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default UserProfile;
