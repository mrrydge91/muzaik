import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const Chat = props => {
  return (
    <div>
      <h1>Chat</h1>
      {props.allMessages && (
        <ul>
          {props.allMessages.map(message => {
            return (
              <li key={message.content}>
                {message.user.email}---{message.content}
              </li>
            );
          })}
        </ul>
      )}

      <TextField
        label={"Message text"}
        value={props.messageText}
        onChange={e => props.setMessageText(e.target.value)}
      />

      <Button onClick={props.submit}>Submit</Button>
      <br />
      <br />
      <br />
    </div>
  );
};

export default Chat;
