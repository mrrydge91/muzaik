import React from "react";

import UserProfile from "./userProfile";

const Profile = ({
  addPic,
  profile,
  editField,
  submitProfile,
  credits,
  products,
  bulletins,
  isUser,
}) => {
  return (
    <UserProfile
      profile={profile}
      addPic={addPic}
      editField={editField}
      credits={credits}
      bulletins={bulletins}
      submitProfile={submitProfile}
      products={products}
      isUser={isUser}
    />
  );
};

export default Profile;
