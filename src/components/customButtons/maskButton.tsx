import React from "react";
import "../../css/maskButton.scss";

const MaskButton = ({ handleClick, text, type }: any) => {
  const buttonSwitch = () => {
    switch (type) {
      case "fadeIn":
        return (
          <div className="button-container-3">
            <span className="mas">{text}</span>
            <button type="button" onClick={handleClick} name="Hover">
              {text}
            </button>
          </div>
        );
      case "brush":
        return (
          <div className="button-container-2">
            <span className="mas">{text}</span>
            <button onClick={handleClick} type="button" name="Hover">
              {text}
            </button>
          </div>
        );
      case "fadeOut":
        return (
          <div className="button-container-1">
            <span className="mas">{text}</span>
            <button onClick={handleClick} id="work" type="button" name="Hover">
              {text}
            </button>
          </div>
        );
      default:
        return (
          <div className="button-container-2">
            <span className="mas">{text}</span>
            <button onClick={handleClick} type="button" name="Hover">
              {text}
            </button>
          </div>
        );
    }
  };

  return <>{buttonSwitch()}</>;
};

export default MaskButton;
