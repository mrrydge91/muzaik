import React from "react";
import "../../css/fancyButton.scss";

const FancyButton = ({ handleClick, text }: any) => {
  return (
    <>
      <button onClick={handleClick} className="button">
        {text}
        <div className="button__horizontal"></div>
        <div className="button__vertical"></div>
      </button>
    </>
  );
};

export default FancyButton;
