import { makeStyles, Typography } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";
import "../../css/spotlight.scss";

const useStyles = makeStyles({
  main: {},
  text: { color: "#000" },
});

const SpotlightLogo = () => {
  const classes = useStyles();
  return (
    <div className={`${classes.main} logo`}>
      <Link to="/">
        {" "}
        <Typography align="center" className={classes.text}>
          Muzaik
        </Typography>
      </Link>
    </div>
  );
};

export default SpotlightLogo;
