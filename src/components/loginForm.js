import React from "react";
import {
  TextField,
  Button,
  Card,
  CardContent,
  Typography,
  makeStyles,
  Grid,
  CardActions,
} from "@material-ui/core/";
import { Link } from "react-router-dom";

const styles = makeStyles({
  courier: {
    fontFamily: "Courier New, Courier, monospace",
    fontWeight: 600,
  },
  mainTitle: {
    fontSize: "4em",
    marginTop: "50px",
  },
  card: {
    margin: "20vh auto",
  },
  google: {
    border: "2px solid #f00",
    "&:hover": {
      color: "#fff",
      background:
        "linear-gradient(red, transparent), linear-gradient(to top left, purple, transparent), linear-gradient(to top right, blue, transparent)",
      backgroundBlendMode: "screen",
    },
  },
  facebook: {
    borderRadius: 0,
    border: "2px solid #00f",
    "&:hover": {
      color: "#fff",
      background:
        "linear-gradient(red, transparent), linear-gradient(to top left, purple, transparent), linear-gradient(to top right, blue, transparent)",
      backgroundBlendMode: "screen",
    },
  },
  emailDiv: {
    margin: "100px auto",
    padding: "50px",
  },
  whoa: {
    border: "2px #000 solid",
    "&:hover": {
      color: "#fff",
      background:
        "linear-gradient(red, transparent), linear-gradient(to top left, purple, transparent), linear-gradient(to top right, blue, transparent)",
      backgroundBlendMode: "screen",
    },
  },
});

const LoginForm = ({
  email,
  password,
  handleName,
  handleEmail,
  handlePassword,
  login,
  google,
  facebook,
  errorMessage,
}) => {
  const classes = styles();
  return (
    <Grid container>
      <Grid item xs={12}>
        <Typography
          align="center"
          className={`${classes.courier} ${classes.mainTitle}`}
        >
          Muzaik
        </Typography>
      </Grid>
      <Card raised className={classes.emailDiv}>
        <CardContent>
          <Grid container>
            <Grid style={{ textAlign: "center", margin: "auto" }} item xs={6}>
              <TextField
                InputProps={{
                  className: `${classes.courier}`,
                }}
                value={email}
                onChange={handleEmail}
                label={
                  <Typography className={classes.courier}>
                    Your Email
                  </Typography>
                }
                error={errorMessage ? true : false}
                helperText={errorMessage}
              />
            </Grid>

            <Grid item xs={6}>
              <TextField
                InputProps={{
                  className: `${classes.courier}`,
                  type: "password",
                }}
                value={password}
                onChange={handlePassword}
                label={
                  <Typography className={classes.courier}>
                    Your Password
                  </Typography>
                }
              />
            </Grid>
          </Grid>
        </CardContent>
        <CardActions>
          <Button
            className={`${classes.whoa} ${classes.courier}`}
            variant="outlined"
            onClick={login}
          >
            Login with email
          </Button>

          <Link to="/register">
            <Button
              className={`${classes.whoa} ${classes.courier}`}
              variant="outlined"
            >
              New User?
            </Button>
          </Link>
        </CardActions>
      </Card>
    </Grid>
  );
};

export default LoginForm;
