import React from "react";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";

const OtherProfile = ({ profile, credits, bulletins, products }) => {
  return (
    <Card>
      <CardHeader title="Profile" />
      <CardContent>
        <Grid container>
          <Grid item md={4}>
            <Card>
              <CardContent>
                <img src={profile.pic} alt="prof" />
              </CardContent>
            </Card>
          </Grid>
          <Grid item md={8}>
            <Card>
              <CardContent>
                {Object.keys(profile).map((fieldType) => {
                  if (
                    fieldType !== "pic" &&
                    fieldType !== "email" &&
                    fieldType !== "bio" &&
                    fieldType !== "uid"
                  ) {
                    return (
                      <React.Fragment key={fieldType}>
                        <Typography>
                          {fieldType}: {profile[fieldType]}
                        </Typography>
                      </React.Fragment>
                    );
                  } else {
                    return null;
                  }
                })}
                {credits.length !== 0 && (
                  <>
                    {credits.map((credit) => {
                      return (
                        <Card>
                          <CardContent>
                            <Link to={`/product/${credit.productId}`}>
                              {credit.productName}..........{credit.role}
                            </Link>
                          </CardContent>
                        </Card>
                      );
                    })}
                  </>
                )}
              </CardContent>
            </Card>
          </Grid>

          <Grid item md={8}>
            <Typography>BIO: {profile.bio}</Typography>
          </Grid>

          <Card>
            <CardHeader title="Bulletins" />
            <CardContent>
              {bulletins.map((bulletin) => {
                return <span key={bulletin}>{bulletin}</span>;
              })}
            </CardContent>
          </Card>

          <div>
            <h1>Products</h1>
            {products &&
              Object.values(products).map((product) => {
                return (
                  <React.Fragment key={product.postKey}>
                    <Link to={`/product/${product.postKey}`}>
                      {product.name}
                    </Link>
                    <br />
                  </React.Fragment>
                );
              })}
          </div>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default OtherProfile;
