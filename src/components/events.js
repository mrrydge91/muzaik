import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { DatePicker, KeyboardTimePicker } from "@material-ui/pickers";
// import ChatContainer from "../containers/chatContainer";
import { Link } from "react-router-dom";
import AutoCompleteContainer from "../containers/autoComplete";
import EventCommentContainer from "../containers/eventCommentContainer";

const Events = ({
  deleteItem,
  eventName,
  setEventName,
  eventDate,
  setEventDate,
  eventDesc,
  setEventDesc,
  eventTime,
  eventLocation,
  setEventTime,
  setEventLocation,
  submitEvent,
  itemsNeeded,
  setItemsNeeded,
  itemText,
  handleItemText,
  events,
  fullEvent,
  chat,
  allAttendees,
  setAllAttendees,
  currentUser,
  addInvite,
  inviteeList,
  cancelEvent,
  editEvent,
}) => {
  return (
    <div>
      <h1>Events</h1>
      {events &&
        events.map((event) => {
          if (!event.deleted) {
            return (
              <Link key={event.postKey} to={`/event/${event.postKey}`}>
                {event.name}
              </Link>
            );
          }
          return null;
        })}
      <div>
        <TextField
          value={eventName}
          onChange={(e) => setEventName(e.target.value)}
          label="Name"
        />
        <br />
        <DatePicker
          autoOk
          orientation="landscape"
          variant="static"
          openTo="date"
          value={eventDate || new Date()}
          onChange={(e) => setEventDate(e)}
        />
        <br />

        <KeyboardTimePicker
          margin="normal"
          label="Time picker"
          value={eventTime || new Date()}
          onChange={(e) => setEventTime(e)}
        />

        <br />
        <TextField
          value={eventDesc}
          onChange={(e) => setEventDesc(e.target.value)}
          label="Desc"
        />
        <br />
        {itemsNeeded && (
          <ul>
            {itemsNeeded.map((item, index) => (
              <li key={item}>
                {item} <Button onClick={() => deleteItem(index)}>Delete</Button>
              </li>
            ))}
          </ul>
        )}
        <TextField
          value={itemText}
          onChange={(e) => handleItemText(e.target.value)}
          label="Add item"
        />
        <Button
          onClick={() =>
            setItemsNeeded(() => {
              return itemsNeeded && itemsNeeded.length > 0
                ? [...itemsNeeded, itemText]
                : [itemText];
            })
          }
        >
          Add Item
        </Button>

        <br />

        <TextField
          value={eventLocation}
          onChange={(e) => setEventLocation(e.target.value)}
          label="Location"
        />
        <br />
        <Button
          variant="contained"
          onClick={(e) => {
            e.preventDefault();
            submitEvent();
          }}
        >
          Submit
        </Button>

        <Button variant="contained" onClick={editEvent}>
          Edit
        </Button>

        <h1>Attendees</h1>
        <h3>{allAttendees.length} going</h3>
        {allAttendees.length > 0 &&
          allAttendees.map((person) => {
            return (
              <Link key={person.uid} to={`/profile/${person.uid}`}>
                Going: {person.name}
              </Link>
            );
          })}
        <Button
          onClick={() => {
            setAllAttendees([
              ...allAttendees,
              { name: currentUser.properties.name, uid: currentUser.uid },
            ]);
          }}
        >
          GO TO EVENT
        </Button>

        <br />

        {inviteeList.length && (
          <ul>
            {inviteeList.map((invitee) => (
              <Link key={invitee.uid} to={`/profile/${invitee.uid}`}>
                {invitee.name}
              </Link>
            ))}
          </ul>
        )}
        <AutoCompleteContainer passedAction={addInvite} />

        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </div>
      <div>
        {fullEvent && (
          <div>
            {/* <ChatContainer
              chat={fullEvent.chat ? fullEvent.chat : []}
              productId={fullEvent.postKey}
              type={"events"}
            /> */}

            <Button
              onClick={() =>
                cancelEvent(
                  fullEvent.postKey,
                  fullEvent.productId && fullEvent.productId
                )
              }
            >
              Cancel
            </Button>
          </div>
        )}
        {fullEvent?.postKey && <EventCommentContainer />}
      </div>
    </div>
  );
};

export default Events;
