import {
  Grid,
  Button,
  Card,
  CardActions,
  CardContent,
  TextField,
  Checkbox,
  FormControlLabel,
  Chip,
} from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";

const Comment = (props) => {
  const {
    allComments,
    isCrit,
    setIsCrit,
    commentText,
    setCommentText,
    submitComment,
  } = props;
  return (
    <Card raised>
      <Grid container>
        <Grid item xs={12}>
          <CardContent>
            {allComments.map((comment, index) => {
              return (
                <Card key={comment.commentText + index}>
                  <CardContent>
                    <Link to={`/profile/${comment.userUID}`}>
                      {comment.userName}
                    </Link>
                    : {comment.commentText}
                    {comment.isCrit && <Chip label="Critique" />}
                  </CardContent>
                </Card>
              );
            })}
            <TextField
              label="Leave a Comment/Critique"
              value={commentText}
              onChange={(e) => setCommentText(e.target.value)}
              variant="outlined"
              style={{ width: "100%" }}
            />
          </CardContent>
        </Grid>
        <CardActions>
          {isCrit && (
            <FormControlLabel
              control={
                <Checkbox
                  checked={isCrit}
                  onChange={() => setIsCrit(!isCrit)}
                  color="primary"
                />
              }
              label="This is a Critique"
            />
          )}
          <Button onClick={submitComment} variant="contained">
            Comment
          </Button>
        </CardActions>
      </Grid>
    </Card>
  );
};

export default Comment;
