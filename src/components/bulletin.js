import React from "react";
import AddBulletinContainer from "../containers/addBulletinContainer";

const Bulletin = (props) => {
  const { bulletins } = props;
  return (
    <div>
      <h1>Bulletin</h1>
      {bulletins.length > 0 &&
        bulletins.map((bulletin) => {
          return (
            <div key={bulletin.message}>
              <span>{bulletin.userName}</span>
              <span>{bulletin.message}</span>
            </div>
          );
        })}
      <AddBulletinContainer />
    </div>
  );
};

export default Bulletin;
