import { REGISTER_NEW_USER } from "../actionTypes/register";

const registerReducer = (state = {}, action) => {
  switch (action.type) {
    case REGISTER_NEW_USER:
      Object.keys(action.user).forEach(key => {
        return {
          ...state,
          [key]: action.user[key]
        };
      });
      return state;
    default:
      return state;
  }
};

export default registerReducer;
