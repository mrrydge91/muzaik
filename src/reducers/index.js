import { combineReducers } from "redux";
import loginReducer from "./loginReducer";
import profileReducer from "./profileReducer";
import productReducer from "./productReducer";
import eventsReducer from "./eventsReducer";
import chatReducer from "./chatReducer";
import appbarReducer from "./appbar";
// import registerReducer from "./registerReducer";

const appReducer = combineReducers({
  user: loginReducer,
  profile: profileReducer,
  product: productReducer,
  events: eventsReducer,
  chat: chatReducer,
  appbar: appbarReducer,
});

export default (state, action) => {
  return appReducer(state, action);
};
