import { ADD_PIC, SET_PROFILE, EDIT_PROFILE } from "../actionTypes/profile";

const profileReducer = (state = null, action) => {
  switch (action.type) {
    case SET_PROFILE:
      let profile = {};
      if (action.profile) {
        Object.keys(action.profile).forEach(key => {
          profile[key] = action.profile[key];
        });
      }

      return profile;
    case ADD_PIC:
      return {
        ...state,
        pic: action.pic
      };
    case EDIT_PROFILE:
      return action.profile;
    default:
      return state;
  }
};

export default profileReducer;
