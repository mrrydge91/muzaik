import { ADD_MESSAGE, CREATE_CHAT } from "../actionTypes/chat";

const chatReducer = (state = null, action) => {
  switch (action.type) {
    case CREATE_CHAT:
      return state;
    case ADD_MESSAGE:
      return state;
    default:
      return state;
  }
};

export default chatReducer;
