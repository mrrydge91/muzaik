import { CHANGE_TAB_VALUE } from "../actionTypes/appbar";

const appbarReducer = (state = { tabValue: 0 }, action) => {
  switch (action.type) {
    case CHANGE_TAB_VALUE:
      return { tabValue: action.tabValue };
    default:
      return state;
  }
};

export default appbarReducer;
