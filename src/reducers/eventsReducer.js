import {
  ADD_EVENT,
  EDIT_EVENT,
  CANCEL_EVENT,
  SET_EVENT
} from "../actionTypes/events";

const eventsReducer = (state = null, action) => {
  switch (action.type) {
    case ADD_EVENT:
      return {
        eventView: action.event
      };
    case SET_EVENT:
      return {
        eventView: action.event
      };
    case EDIT_EVENT:
      return state;
    case CANCEL_EVENT:
      return state;
    default:
      return state;
  }
};

export default eventsReducer;
