import { LOGOUT, LOGIN, UPDATE_USER } from "../actionTypes/login";

const initState = {
  authed: false,
  email: null,
  uid: null
};

const loginReducer = (state = initState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        email: action.email,
        uid: action.uid,
        authed: true
      };
    case UPDATE_USER:
      return {
        ...state,
        properties: action.user
      };
    case LOGOUT:
      return initState;
    default:
      return state;
  }
};

export default loginReducer;
