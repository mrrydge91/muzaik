import { ADD_PRODUCT, SET_PRODUCT, EDIT_PRODUCT } from "../actionTypes/product";
import update from "immutability-helper";

let initState = {
  products: [],
  productView: null
};

const productReducer = (state = initState, action) => {
  switch (action.type) {
    case ADD_PRODUCT:
      return {
        ...state,
        products: update(state.products, {
          $push: [action.product]
        })
      };
    case SET_PRODUCT:
      return {
        ...state,
        productView: action.product
      };
    case EDIT_PRODUCT:
      return {
        ...state,
        productView: action.product
      };
    default:
      return state;
  }
};

export default productReducer;
