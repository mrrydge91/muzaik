import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useRouteMatch } from "react-router-dom";
import Comment from "../components/comment";
import FirebaseUtils from "../utils/firebaseUtils";

const ProductCommentContainer = (props) => {
  const [isCrit, setIsCrit] = useState(false);
  const [commentText, setCommentText] = useState("");
  const [allComments, setAllComments] = useState([]);

  const userUID = useSelector((state) => state.user?.uid);
  const userName = useSelector((state) => state.user?.properties?.name);
  const productId = useSelector((state) => state.product?.productView?.postKey);
  const productUser = useSelector((state) => state.product?.productView?.user);
  const match = useRouteMatch();

  const productComments = useSelector(
    (state) => state.product?.productView?.comments
  );

  useEffect(() => {
    if (match.params.postId === "new") {
      setAllComments([]);
    }
  }, [match]);

  useEffect(() => {
    if (productComments?.length) {
      setAllComments(
        productUser === userUID
          ? productComments
          : productComments.filter((comment) => !comment.isCrit)
      );
    }
  }, [productId, productUser, userUID, productComments]);

  const submitComment = () => {
    const fullComment = {
      userUID,
      userName,
      isCrit,
      commentText,
      productId,
    };

    const newComments = [...allComments, fullComment];

    setAllComments(newComments);

    FirebaseUtils.firestore.addComment(productId, newComments);
  };

  return (
    <Comment
      allComments={allComments}
      isCrit={isCrit}
      setIsCrit={setIsCrit}
      commentText={commentText}
      setCommentText={setCommentText}
      submitComment={submitComment}
    />
  );
};

export default ProductCommentContainer;
