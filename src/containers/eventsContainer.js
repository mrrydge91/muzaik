import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import Events from "../components/events";
import { connect } from "react-redux";
import { addEvent, editEvent, cancelEvent, setEvent } from "../actions/events";
import firebase from "../config/firebaseConfig";
import { Card, CardContent } from "@material-ui/core";

const EventsContainer = ({
  match,
  addEvent,
  user,
  location,
  editEvent,
  events,
  cancelEvent,
}) => {
  const [eventName, setEventName] = useState("");
  const [eventDate, setEventDate] = useState("");
  const [eventTime, setEventTime] = useState("");
  const [eventDesc, setEventDesc] = useState("");
  const [eventLocation, setEventLocation] = useState("");
  const [itemsNeeded, setItemsNeeded] = useState(null);
  const [itemText, handleItemText] = useState("");
  const [fullEvent, setFullEvent] = useState(null);
  const [allAttendees, setAllAttendees] = useState([]);
  const [inviteeList, setInviteeList] = useState([]);

  useEffect(() => {
    if (match.path === "/event/:eventId" && match.url !== "/event/new") {
      firebase
        .firestore()
        .collection("events")
        .doc(`${match.params.eventId}`)
        .get()
        .then((resp) => {
          setFullEvent(resp.data());

          addEvent(resp.data());
        });
    }
  }, [match, addEvent]);

  useEffect(() => {
    if (fullEvent) {
      setEventName(fullEvent.name);
      setEventDate(fullEvent.date);
      setEventTime(fullEvent.time);
      setEventDesc(fullEvent.desc);
      setItemsNeeded(fullEvent.itemsNeeded);
      setEventLocation(fullEvent.location);
      setAllAttendees(fullEvent.allAttendees);
    }
  }, [fullEvent]);

  const addInvite = (e, value) => {
    setInviteeList(value);
  };

  const submitEvent = () => {
    const fullLocalEvent = {
      name: eventName,
      date: `${eventDate}`,
      time: `${eventTime}`,
      desc: eventDesc,
      userUID: user.uid,
      userName: user.properties.name,
      location: eventLocation,
      itemsNeeded,
      allAttendees,
    };
    if (location.state?.productId) {
      fullLocalEvent.productId = location.state?.productId;
    }
    setFullEvent(fullLocalEvent);
    addEvent(fullLocalEvent);
  };

  const localEditEvent = () => {
    const editedEvent = {
      name: eventName,
      date: `${eventDate}`,
      time: `${eventTime}`,
      desc: eventDesc,
      user: { uid: user.uid, name: user.properties.name },
      location: eventLocation,
      itemsNeeded,
      productId: fullEvent.productId
        ? fullEvent.productId
        : match.params.eventId || null,
      allAttendees,
      postKey: fullEvent.postKey,
    };
    editEvent(editedEvent);
  };

  const deleteItem = (index) => {
    let allItems = [...itemsNeeded];
    allItems.splice(index, 1);
    setItemsNeeded(allItems);
  };

  return (
    <Card>
      <CardContent>
        <Events
          deleteItem={deleteItem}
          events={events ? events : []}
          eventName={eventName}
          setEventName={setEventName}
          eventDate={eventDate}
          setEventDate={setEventDate}
          eventDesc={eventDesc}
          setEventDesc={setEventDesc}
          eventTime={eventTime}
          eventLocation={eventLocation}
          setEventTime={setEventTime}
          setEventLocation={setEventLocation}
          submitEvent={submitEvent}
          itemsNeeded={itemsNeeded}
          setItemsNeeded={setItemsNeeded}
          itemText={itemText}
          handleItemText={handleItemText}
          fullEvent={fullEvent}
          allAttendees={allAttendees}
          setAllAttendees={setAllAttendees}
          currentUser={user}
          addInvite={addInvite}
          inviteeList={inviteeList}
          cancelEvent={cancelEvent}
          editEvent={localEditEvent}
        />
      </CardContent>
    </Card>
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
  event: state.events,
});

const mapDispatchToProps = (dispatch) => ({
  addEvent: (event) => dispatch(addEvent(event)),
  editEvent: (event) => dispatch(editEvent(event)),
  cancelEvent: (eventId) => dispatch(cancelEvent(eventId)),
  setEvent: (event) => dispatch(setEvent(event)),
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(EventsContainer)
);
