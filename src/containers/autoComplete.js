import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import firebase from "../config/firebaseConfig";

const AutoCompleteContainer = (props) => {
  const [userList, setUserList] = useState([]);

  const getUsers = () => {
    const allUsers = [];
    firebase
      .firestore()
      .collection("users")
      .get()
      .then((resp) => {
        resp.forEach((user) => {
          allUsers.push(user.data());
        });
        setUserList(allUsers);
      });
  };

  useEffect(() => {
    getUsers();
  }, []);

  return (
    <Autocomplete
      freeSolo
      onChange={(e, value) => {
        props.passedAction(e, value);
      }}
      id="combo-box-demo"
      getOptionLabel={(option) => option.name || option.email}
      options={userList}
      renderInput={(params) => (
        <TextField
          onChange={(e, value) => {
            props.passedAction(e, value);
          }}
          {...params}
          label="Person's name"
          variant="outlined"
          fullWidth
        />
      )}
    />
  );
};

export default AutoCompleteContainer;
