import { Card, CardContent, Typography } from "@material-ui/core";
import React from "react";

const AboutContainer = () => {
  return (
    <Card>
      <CardContent>
        <Typography align="center">
          Muzaik is a place to show off your creative projects. This project's
          goal is to celebrate the creatives of Pittsburgh. We work damn hard
          and all for other people to enjoy. This is for us. My name is Kevin
          Ridge and I built this website. I wanted to show off the things I've
          made while encouraging artists to collaborate all over the city. I
          love local art and I want to show my gratitude to all of the invisible
          work that artists do.
        </Typography>
        <Typography>
          This project is very bare bones as of now (I'm only one guy!) but here
          are some ideas of what's coming:
          <ul>
            <li>Upload progress pictures of your projects</li>
            <li>Comment AND critique projects (critiques are private)</li>
            <li>Create and track events</li>
            <li>
              Post events for a specific project (shoot days for a movie,
              recording days for a song)
            </li>
            <li>Chat feature (ya know, like AIM)</li>
            <li>
              Video chat (with shared docs, presenting modes, notes section)
            </li>
          </ul>
        </Typography>
        <Typography>
          What I need help with:
          <ul>
            <li>Design</li>
            <li>I need feedback to help the community more</li>
          </ul>
        </Typography>
      </CardContent>
    </Card>
  );
};

export default AboutContainer;
