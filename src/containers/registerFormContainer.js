import React, { useState } from "react";
import RegisterForm from "../components/registerForm";
import { login } from "../actions/login";
import { connect } from "react-redux";
import firebase from "../config/firebaseConfig";
import { registerNewUser } from "../actions/register";

const RegisterFormContainer = (props) => {
  // const [errorMessage, setErrorMessage] = useState("");
  const [user, setUser] = useState({});
  const [password, setPassword] = useState("");

  const facebookLogin = () => {
    let provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().signInWithRedirect(provider);
    props.history.push(`/main`);
  };

  const googleLogin = () => {
    let provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithRedirect(provider);
    props.history.push(`/main`);
  };

  const newRegister = () => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(user.email, password)
      .then((response) => {
        let newUser = {
          uid: response.user.uid,
          email: response.user.email,
          name: response.user.email,
        };

        props.registerNewUser(newUser);
        props.login(newUser.email);
        setUser({
          errorMessage: "",
          email: "",
          name: "",
          bio: "",
          hometown: "",
          password: "",
        });
        props.history.push(`/main`);
      });
  };

  const handleText = (e, type) => {
    setUser({
      ...user,
      [type]: e.target.value,
    });
  };

  const handlePassword = (e) => {
    setPassword(e.target.value);
  };

  return (
    <RegisterForm
      newRegister={newRegister}
      handlePassword={handlePassword}
      handleText={handleText}
      state={user}
      password={password}
      facebook={facebookLogin}
      google={googleLogin}
    />
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
  login: (email) => dispatch(login(email)),
  registerNewUser: (statePkg) => dispatch(registerNewUser(statePkg)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterFormContainer);
