import React, { useState } from "react";
import { connect } from "react-redux";
import AddBulletin from "../components/addBulletin";
import firebase from "../config/firebaseConfig";

const AddBulletinContainer = ({ user }) => {
  const [bulletinText, setBulletinText] = useState("");

  const submitBulletin = () => {
    const bulletin = {
      message: bulletinText,
      userUID: user.uid,
      userName: user.properties.name || "no name",
      timeCreated: new Date(),
    };
    firebase
      .firestore()
      .collection("bulletins")
      .add(bulletin)
      .then((resp) => {
        firebase
          .firestore()
          .collection("users")
          .doc(user.uid)
          .update({
            bulletins: firebase.firestore.FieldValue.arrayUnion(resp.id),
          });
      })
      .catch((e) => console.error("adding bulletin", e));
  };
  return (
    <AddBulletin
      bulletinText={bulletinText}
      setBulletinText={setBulletinText}
      submit={submitBulletin}
    />
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(AddBulletinContainer);
