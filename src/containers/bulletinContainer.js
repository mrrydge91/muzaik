import React, { useState, useEffect } from "react";
import firebase from "../config/firebaseConfig";
import Bulletin from "../components/bulletin";

const BulletinContainer = () => {
  const [bulletins, setBulletins] = useState([]);

  const getBulletins = () => {
    const tempBulletins = [];
    firebase
      .firestore()
      .collection("bulletins")
      .limit(20)
      .get()
      .then((resp) => {
        resp.forEach((bulletin) => {
          tempBulletins.push(bulletin.data());
        });
        tempBulletins.sort((a, b) => {
          return a.timeCreated - b.timeCreated;
        });
        setBulletins(tempBulletins);
      });
  };

  useEffect(() => {
    getBulletins();
  }, []);

  return <Bulletin bulletins={bulletins} />;
};

export default BulletinContainer;
