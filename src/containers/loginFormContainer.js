import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import LoginForm from "../components/loginForm";
import { login, updateUser } from "../actions/login";
import { connect } from "react-redux";
import firebase from "../config/firebaseConfig";

const LoginFormContainer = (props) => {
  const [errorMessage, setErrorMessage] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const facebookLogin = () => {
    let provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().signInWithRedirect(provider);
  };

  const googleLogin = () => {
    let provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithRedirect(provider);
  };

  const login = () => {
    firebase
      .auth()
      .signInWithEmailAndPassword(`${email}`, `${password}`)
      .then((response) => {
        setErrorMessage("");
        setEmail("");
        setPassword("");
        props.login(email);
      })
      .catch(function (error) {
        setErrorMessage(error.message);
        setEmail("");
        setPassword("");
        alert(error.message);
      });
  };

  const handleEmail = (e) => {
    setEmail(e.target.value);
  };

  const handlePassword = (e) => {
    setPassword(e.target.value);
  };

  const { from } = props.location?.state || { from: { pathname: "/" } };
  const { user } = props;
  if (user.authed) {
    return <Redirect to={from} />;
  }

  return (
    <LoginForm
      handlePassword={handlePassword}
      handleEmail={handleEmail}
      login={login}
      email={email}
      password={password}
      google={googleLogin}
      facebook={facebookLogin}
      errorMessage={errorMessage}
    />
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  login: (email) => dispatch(login(email)),
  updateUser: (uid) => dispatch(updateUser(uid)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginFormContainer);
