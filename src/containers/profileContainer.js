import React, { useEffect, useState } from "react";
import Profile from "../components/profile";
import { connect } from "react-redux";
import { setProfile, addPic, editProfile } from "../actions/profile";
import firebase from "../config/firebaseConfig";
import { useLocation } from "react-router-dom";

const ProfileContainer = ({
  editProfile,
  user,
  setProfile,
  addPic,
  profile,
}) => {
  const [selectedUser, setSelectedUser] = useState({});
  const [credits, setCredits] = useState([]);
  const [products, setProducts] = useState([]);
  const [bulletins, setBulletins] = useState([]);
  const location = useLocation();

  useEffect(() => {
    firebase
      .firestore()
      .collection(`users`)
      .doc(`${location.pathname.split("/")[2]}`)
      .get()
      .then((doc) => {
        if (doc.exists) {
          setSelectedUser({
            email: doc.data().email || "",
            name: doc.data().name || "",
            hometown: doc.data().hometown || "",
            bio: doc.data().bio || "",
            pic: doc.data().pic || "",
            uid: doc.data().uid || "",
            events: doc.data().events || [],
          });

          setCredits(doc.data().credits || []);
          setProducts(doc.data().products || []);
          setBulletins(doc.data().bulletins || []);
        }
      })
      .catch((error) => {
        alert(error.message);
      });
  }, [location.pathname]);

  const editField = (e, type) => {
    e.preventDefault();
    setSelectedUser({
      ...selectedUser,
      [type]: e.target.value,
    });
  };

  const submitProfile = (e) => {
    e.preventDefault();

    editProfile(selectedUser);
  };

  const addPicLocal = (e) => {
    var fr = new FileReader();
    var files = e.target.files;
    fr.onload = function () {
      setSelectedUser({
        ...selectedUser,
        pic: fr.result,
      });
    };
    fr.readAsDataURL(files[0]);
    e.preventDefault();

    firebase
      .storage()
      .ref(`/users/${user.uid}/pic`)
      .put(e.target.files[0])
      .then((snapshot) => {
        snapshot.ref.getDownloadURL().then((url) => {
          addPic(url, user.uid);
        });
      });
  };

  if (selectedUser.email) {
    return (
      <Profile
        editField={editField}
        submitProfile={submitProfile}
        addPic={addPicLocal}
        profile={selectedUser}
        credits={credits}
        products={products}
        bulletins={bulletins}
        isUser={selectedUser.uid === user.uid}
      />
    );
  } else {
    return <h1>no profile</h1>;
  }
};

const mapStateToProps = (state) => ({
  user: state.user,
  profile: state.profile,
});

const mapDispatchToProps = (dispatch) => ({
  setProfile: (email) => dispatch(setProfile(email)),
  addPic: (pic, uid) => dispatch(addPic(pic, uid)),
  editProfile: (profile) => dispatch(editProfile(profile)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer);
