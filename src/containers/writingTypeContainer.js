import { TextField } from "@material-ui/core";
import React from "react";
// import { createEditor } from "slate";
// import { Slate, Editable, withReact } from "slate-react";

const WritingTypeContainer = ({ writingArea, setWritingArea }) => {
  // const editor = useMemo(() => withReact(createEditor()), []);

  return (
    <>
      <h1>Writing</h1>{" "}
      {/* <Slate
        editor={editor}
        value={writingArea}
        onChange={(newValue) => setWritingArea(newValue)}
      >
        <Editable /> 
      </Slate> */}
      <TextField
        value={writingArea}
        label={"Just start writing"}
        multiline
        onChange={setWritingArea}
      />
    </>
  );
};

export default WritingTypeContainer;
