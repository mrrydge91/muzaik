import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import Chat from "../components/chat.js";
// import { createChat, addMessage } from "../actions/chat";
import firebase from "../config/firebaseConfig";

const ChatContainer = (props) => {
  const [allMessages, setAllMessages] = useState([]);
  const [messageText, setMessageText] = useState("");

  useEffect(() => {
    if (allMessages.length) {
      uploadChat();
    }
  }, [allMessages]);

  useEffect(() => {
    if (!allMessages.length && props.chat) {
      setAllMessages(props.chat);
    }
  });

  const uploadChat = () => {
    firebase
      .firestore()
      .collection(props.type)
      .doc(`${props.productId}`)
      .update({ chat: allMessages });
  };

  const submit = () => {
    setAllMessages((allMessages) => [
      ...allMessages,
      {
        content: messageText,
        chatId: allMessages.length,
        user: { email: props.user.email, uid: props.user.uid },
      },
    ]);
  };

  return (
    <Chat
      messageText={messageText}
      setMessageText={setMessageText}
      submit={submit}
      allMessages={allMessages}
    />
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(ChatContainer);
