import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useRouteMatch } from "react-router-dom";
import Comment from "../components/comment";
import FirebaseUtils from "../utils/firebaseUtils";

const EventCommentContainer = () => {
  const [commentText, setCommentText] = useState("");
  const [allComments, setAllComments] = useState([]);

  const userUID = useSelector((state) => state.user?.uid);
  const userName = useSelector((state) => state.user?.properties?.name);
  const eventId = useSelector((state) => state.events?.eventView?.postKey);
  const eventUser = useSelector((state) => state.events?.eventView?.user);
  const match = useRouteMatch();

  const eventComments = useSelector(
    (state) => state.events?.eventView?.comments
  );

  useEffect(() => {
    if (match.params.postId === "new") {
      setAllComments([]);
    }
  }, [match]);

  useEffect(() => {
    if (eventComments) {
      setAllComments(eventComments);
    }
  }, [eventId, eventUser, userUID, eventComments]);

  const submitComment = () => {
    const fullComment = {
      userUID,
      userName,
      commentText,
      eventId,
    };
    const newComments = [...allComments, fullComment];
    setAllComments(newComments);
    FirebaseUtils.firestore.addEventComment(eventId, newComments);
  };

  return (
    <Comment
      allComments={allComments}
      commentText={commentText}
      setCommentText={setCommentText}
      submitComment={submitComment}
    />
  );
};

export default EventCommentContainer;
