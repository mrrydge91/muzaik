import React, { useState, useEffect, useCallback } from "react";
import Product from "../components/product";
import { connect } from "react-redux";
import { addProduct, setProduct, editProduct } from "../actions/product";
import firebase from "../config/firebaseConfig";
import { useLocation, useRouteMatch } from "react-router-dom";

const initialProperties = {
  credits: [],
  name: "",
  type: "",
  dateCreated: "",
  lastEdit: "",
  videoLink: "",
  desc: "",
  pic: "",
  githubLink: "",
  writingArea: "",
};

const ProductContainer = ({
  setProduct,
  product,
  editProduct,
  user,
  addProduct,
}) => {
  const [productProperties, setProductProperties] = useState(initialProperties);
  const [creditPersonText, setCreditPersonText] = useState("");
  const [creditRoleText, setCreditRoleText] = useState("");
  const [events, setEvents] = useState([]);
  const [picURL, setPicURL] = useState("");
  const location = useLocation();
  const match = useRouteMatch();

  const setWritingArea = (e) => {
    e.preventDefault();
    setProductProperties({
      ...productProperties,
      writingArea: e.target.value,
    });
  };

  const setProductLocal = useCallback(
    (data) => {
      setProduct(data);
      setProductProperties({
        credits: data.credits || initialProperties.credits,
        name: data.name || initialProperties.name,
        type: data.type || initialProperties.type,
        dateCreated: data.dateCreated || initialProperties.dateCreated,
        lastEdit: data.lastEdit || initialProperties.lastEdit,
        videoLink: data.videoLink || initialProperties.videoLink,
        desc: data.desc || initialProperties.desc,
        user: data.user || "",
        pic: data.pic || initialProperties.pic,
        githubLink: data.githubLink || initialProperties.githubLink,
        writingArea: data.writingArea || initialProperties.writingArea,
        events: data.events || [],
      });
    },
    [setProduct]
  );

  useEffect(() => {
    if (match.params.postId !== "new") {
      firebase
        .firestore()
        .collection("products")
        .doc(`${location.pathname.split("/")[2]}`)
        .get()
        .then((doc) => {
          setProductLocal(doc.data());
          setEvents(doc.data().events ? doc.data().events : []);
        });
    } else {
      setProductLocal({});
    }
  }, [location.pathname, match.params.postId, setProductLocal]);

  const handleTextChange = (e, property) => {
    setProductProperties({
      ...productProperties,
      [property]: e.target.value,
    });
  };

  const handleCreditPersonTextChange = (e, value) => {
    e.preventDefault();
    setCreditPersonText(value ? value.name || value.email : e.target.value);
  };

  const handleCreditRoleTextChange = (e) => {
    e.preventDefault();
    setCreditRoleText(e.target.value);
  };

  const addPic = (e) => {
    var fr = new FileReader();
    var files = e.target.files;
    fr.onload = function () {
      setProductProperties({
        ...productProperties,
        pic: fr.result,
      });
    };
    fr.readAsDataURL(files[0]);
    e.preventDefault();

    firebase
      .storage()
      .ref(`/products/${user.uid}/pic`)
      .put(e.target.files[0])
      .then((snapshot) => {
        snapshot.ref.getDownloadURL().then((url) => {
          setPicURL(url);
        });
      });
  };

  const addCredit = () => {
    const fullCredits = productProperties.credits;
    firebase
      .firestore()
      .collection("users")
      .where("name", "==", creditPersonText)
      .get()
      .then((resp) => {
        if (resp.docs.length) {
          resp.forEach((doc) => {
            fullCredits.push({
              person: creditPersonText,
              role: creditRoleText,
              uid: doc.data().uid,
            });
            firebase
              .firestore()
              .collection("users")
              .doc(doc.data().uid)
              .update({
                credits: firebase.firestore.FieldValue.arrayUnion({
                  role: creditRoleText,
                  name: productProperties.name,
                  type: productProperties.type,
                  postKey: product.productView
                    ? product.productView.postKey
                    : "",
                }),
              })
              .catch((e) => {
                console.error(e);
              });
            firebase
              .firestore()
              .collection("products")
              .doc(product.productView.postKey)
              .update({
                credits: firebase.firestore.FieldValue.arrayUnion({
                  person: creditPersonText,
                  role: creditRoleText,
                  uid: doc.data().uid,
                }),
              })
              .catch((e) => {
                console.error(e);
              });
            setProductProperties({
              ...productProperties,
              credits: fullCredits,
            });
          });
        } else {
          setProductProperties({
            ...productProperties,
            credits: [
              ...productProperties.credits,
              { person: creditPersonText, role: creditRoleText },
            ],
          });
        }
      })
      .catch((e) => {
        console.error("HERE", e);
      });
  };

  const deleteCredit = (rowData) => {
    const allCredits = [...productProperties.credits];
    setProductProperties({
      ...productProperties,
      credits: allCredits.filter(
        (credit) =>
          credit.role !== rowData.role && credit.name !== rowData.person
      ),
    });
  };

  const editProductLocal = () => {
    const productLocal = {
      name: productProperties.name,
      type: productProperties.type,
      desc: productProperties.desc,
      user: product.productView.user,
      postKey: product.productView.postKey,
      dateCreated: product.productView.dateCreated,
      credits: productProperties.credits,
      videoLink: productProperties.videoLink,
      githubLink: productProperties.githubLink,
      lastEdit: new Date(),
      pic: picURL,
      writingArea: productProperties.writingArea,
    };
    editProduct(productLocal);
  };

  const addProductLocal = (e) => {
    e.preventDefault();
    const product = {
      name: productProperties.name,
      type: productProperties.type,
      desc: productProperties.desc,
      user: user.uid,
      credits: productProperties.credits,
      videoLink: productProperties.videoLink,
      githubLink: productProperties.githubLink,
      dateCreated: new Date(),
      pic: picURL,
      writingArea: productProperties.writingArea,
    };
    addProduct(product);
  };

  return (
    <Product
      deleteCredit={deleteCredit}
      editProduct={editProductLocal}
      addProduct={addProductLocal}
      properties={productProperties}
      handleTextChange={handleTextChange}
      handleCreditPersonTextChange={handleCreditPersonTextChange}
      handleCreditRoleTextChange={handleCreditRoleTextChange}
      addCredit={addCredit}
      productId={product.productView ? product.productView.postKey : ""}
      creditPersonText={creditPersonText}
      creditRoleText={creditRoleText}
      events={events}
      isUser={productProperties.user === user.uid}
      isNew={location.pathname.split("/")[2] === "new"}
      signedIn={user.uid ? true : false}
      addPic={addPic}
      setWritingArea={setWritingArea}
    />
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
  product: state.product,
});

const mapDispatchToProps = (dispatch) => ({
  addProduct: (product) => dispatch(addProduct(product)),
  setProduct: (product) => dispatch(setProduct(product)),
  editProduct: (product) => dispatch(editProduct(product)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductContainer);
