import React from "react";
import { connect } from "react-redux";
import Appbar from "../components/appbar";
import { logout } from "../actions/login";
import { setTabValue } from "../actions/appbar";

const AppbarContainer = ({ uid, logout, setTabValue, tabValue }) => {
  const handleChange = (event, value) => {
    setTabValue(value);
  };

  return (
    <Appbar
      uid={uid}
      logout={logout}
      tabValue={tabValue}
      handleChange={handleChange}
    />
  );
};

const mapStateToProps = (state) => ({
  uid: state.user.uid,
  tabValue: state.appbar.tabValue,
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout()),
  setTabValue: (tabValue) => dispatch(setTabValue(tabValue)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AppbarContainer);
